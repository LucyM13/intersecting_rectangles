# Intersecting_Rectangles

##How to use it ?

1 - Clone the project

2 - In the project home, execute in this order:

    ./gradlew clean && ./gradlew build && ./gradlew runJar

  - './gradlew clean': Deletes the build repository
  - './gradlew build': Build the project
  - './gradlew runJar': Create a jar with all files of the project

3 - Then,
    
    java -jar build/libs/IntersectingRectangles-1.0-SNAPSHOT.jar [file]

