package com.mambre.IntersectingRectangles;

import com.mambre.IntersectingRectangles.interfaces.IFile;
import org.json.simple.parser.JSONParser;
import java.io.FileReader;

public class JsonFile implements IFile {

    private JSONParser jsonParser = new JSONParser();
    private String file;

    public JsonFile(String file) {
        this.file = file;
    }

    @Override
    public boolean validContent() {
        try {
            FileReader fileReader = new FileReader(file);
            if (jsonParser.parse(fileReader) == null) {
                fileReader.close();
                return false;
            }
            fileReader.close();
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean validExtension() {
        return file.endsWith(".json");
    }

    @Override
    public Object readData() {
        Object content;
        try {
            FileReader fileReader = new FileReader(file);
            content = jsonParser.parse(fileReader);
            fileReader.close();
        } catch (Exception e) {
            return null;
        }
        return content;
    }
}
