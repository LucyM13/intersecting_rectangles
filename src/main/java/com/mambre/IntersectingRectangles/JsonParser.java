package com.mambre.IntersectingRectangles;

import com.mambre.IntersectingRectangles.builders.RectangleBuilder;
import com.mambre.IntersectingRectangles.interfaces.IFile;
import com.mambre.IntersectingRectangles.interfaces.IParser;
import com.mambre.IntersectingRectangles.models.Rectangle;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class JsonParser implements IParser {

    public JsonParser() {}

    @Override
    public IFile getFile(String file) {
        return new JsonFile(file);
    }

    @Override
    public ArrayList<Rectangle> getRectangles(Object data, String key, int max) {
        /* Convert data with object type to JSONObject type. */
        JSONObject jsonObject = (JSONObject) data;

        /* Get the corresponding array with the specific key. */
        JSONArray rectangles = (JSONArray) jsonObject.get(key);

        /* Check if the key is found. */
        if (rectangles == null) {
            System.err.println("Error: Do not find the key [" + key + "] inside the file.");
            return null;
        }

        /* Store data and save only x rectangles. */
        ArrayList<Rectangle> rects = new ArrayList<>();
        Iterator i = rectangles.iterator();
        int j = 0;

        while (i.hasNext()) {
            if (j == max) {
                return rects;
            }
            JSONObject currentRectangle = (JSONObject) i.next();
            Rectangle rectangle = new RectangleBuilder()
                    .setX(Integer.parseInt(currentRectangle.get("x").toString()))
                    .setY(Integer.parseInt(currentRectangle.get("y").toString()))
                    .setWidth(Integer.parseInt(currentRectangle.get("delta_x").toString()))
                    .setHeight(Integer.parseInt(currentRectangle.get("delta_y").toString()))
                    .build();
            rects.add(rectangle);
            j++;
        }

        if (rects.size() < 2) {
            System.err.println("Error: You must have two or more rectangles.");
            return null;
        }
        return rects;
    }
}
