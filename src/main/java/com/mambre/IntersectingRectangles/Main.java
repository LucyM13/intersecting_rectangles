package com.mambre.IntersectingRectangles;

import com.mambre.IntersectingRectangles.managers.Manager;
import com.mambre.IntersectingRectangles.models.Intersection;
import com.mambre.IntersectingRectangles.models.Rectangle;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        /* Check number of arguments */
        if (args.length != 1) {
            System.err.println("Error: You must give an input file.");
            System.exit(1);
        }

        Manager manager = new Manager();

        /* Want to parse in JSON file */
        ArrayList<Rectangle> rects = manager.parse(args[0]);

        if (rects == null) {
            System.exit(1);
        }

        /* Record all rectangles from the file */
        System.out.println("Input:");
        for (int i = 0; i < rects.size(); i++) {
            System.out.println("\t" + (i + 1) + rects.get(i));
        }

        /* Search intersections */
        ArrayList<Intersection> list = manager.getIntersections(rects);

        /* Record all intersections found */
        System.out.println("\nOutput:");
        for (int i = 0; i < list.size(); i++) {
            System.out.println("\t" + (i + 1) + list.get(i));
        }

    }
}
