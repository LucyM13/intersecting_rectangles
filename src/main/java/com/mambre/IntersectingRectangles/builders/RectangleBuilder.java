package com.mambre.IntersectingRectangles.builders;

import com.mambre.IntersectingRectangles.interfaces.IRectangle;
import com.mambre.IntersectingRectangles.models.Rectangle;

public class RectangleBuilder implements IRectangle {
    private Integer x;
    private Integer y;
    private Integer width;
    private Integer height;

    public RectangleBuilder() {}

    @Override
    public RectangleBuilder setX(Integer x) {
        this.x = x;
        return this;
    }

    @Override
    public RectangleBuilder setY(Integer y) {
        this.y = y;
        return this;
    }

    @Override
    public RectangleBuilder setWidth(Integer width) {
        this.width = width;
        return this;
    }

    @Override
    public RectangleBuilder setHeight(Integer height) {
        this.height = height;
        return this;
    }

    @Override
    public Rectangle build() {
        return new Rectangle(this.x, this.y, this.width, this.height);
    }
}
