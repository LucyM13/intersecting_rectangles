package com.mambre.IntersectingRectangles.factories;

import com.mambre.IntersectingRectangles.JsonParser;
import com.mambre.IntersectingRectangles.interfaces.IParser;

public class JsonFactory extends ParserAbstractFactory {
    private static JsonFactory instance = null;

    private JsonFactory() {}

    public static synchronized JsonFactory getInstance() {
        if (instance == null) {
            instance = new JsonFactory();
        }
        return instance;
    }

    @Override
    public IParser getParser() {
        return new JsonParser();
    }
}
