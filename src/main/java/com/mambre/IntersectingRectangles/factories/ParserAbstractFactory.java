package com.mambre.IntersectingRectangles.factories;

import com.mambre.IntersectingRectangles.enums.FileType;
import com.mambre.IntersectingRectangles.interfaces.IParser;

public abstract class ParserAbstractFactory {

    public abstract IParser getParser();

    public static ParserAbstractFactory getFactory(FileType type) {

        ParserAbstractFactory parser;

        switch (type) {
            case JSON:
                parser = JsonFactory.getInstance();
                break;
            //case XML:
            default:
                throw new IllegalArgumentException("No such file type");
        }
        return parser;
    }
}
