package com.mambre.IntersectingRectangles.interfaces;

public interface IFile {
    boolean validExtension();
    boolean validContent();
    Object readData();
}
