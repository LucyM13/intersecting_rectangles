package com.mambre.IntersectingRectangles.interfaces;

import com.mambre.IntersectingRectangles.models.Rectangle;

import java.util.ArrayList;

public interface IParser {
    IFile getFile(String file);
    ArrayList<Rectangle> getRectangles(Object data, String key, int max);
}
