package com.mambre.IntersectingRectangles.interfaces;

import com.mambre.IntersectingRectangles.builders.RectangleBuilder;
import com.mambre.IntersectingRectangles.models.Rectangle;

public interface IRectangle {
    RectangleBuilder setX(Integer x);
    RectangleBuilder setY(Integer y);
    RectangleBuilder setWidth(Integer width);
    RectangleBuilder setHeight(Integer height);
    Rectangle build();
}
