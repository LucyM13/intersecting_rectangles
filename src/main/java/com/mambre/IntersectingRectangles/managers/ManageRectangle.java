package com.mambre.IntersectingRectangles.managers;

import com.mambre.IntersectingRectangles.models.Intersection;
import com.mambre.IntersectingRectangles.models.Rectangle;

import java.util.ArrayList;

public class ManageRectangle {
    public ManageRectangle() {}

    /** Verify if the intersection is already found **/
    private Boolean alreadyStore(ArrayList<Intersection> inters, Intersection i) {
        for (Intersection inter : inters) {
            if (i.isEqualsTo(inter)) {
                return true;
            }
        }
        return false;
    }

    /** Try to find intersections among rectangles **/
    private void findIntersection(ArrayList<Rectangle> rects, ArrayList<Intersection> inters, Rectangle A) {
        for (Rectangle B: rects) {

            /* --> A.equals(B) means testing the equality of their pointer whereas A.isEqualTo(B) means testing of their values */
            if (A.hasIntersectionWith(B) && !A.equals(B)) {
                Intersection inter;

                /* Verify if A is a Rectangle of Intersection */
                if (A instanceof Intersection) {
                    inter = new Intersection((Intersection) A, B);
                } else {
                    inter = new Intersection(A, B);
                }

                /* Check if the height and width is not 0 and if the intersection already exists */
                if (inter.getWidth() != 0 && inter.getHeight() != 0 && !alreadyStore(inters, inter)) {
                    inters.add(inter);
                } else {
                    inter.delete();
                }
            }
        }
    }

    /** Search all possible intersections between rectangles **/
    public ArrayList<Intersection> search(ArrayList<Rectangle> rects) {
        ArrayList<Intersection> inters = new ArrayList<>();

        /* Find the first level of intersections */
        for (Rectangle A: rects) {
            findIntersection(rects, inters, A);
        }

        /* Find others level */
        boolean loop = true;
        int j;
        while (loop) {
            j = inters.size();
            for (int k = 0; k < inters.size(); k++) {
                findIntersection(rects, inters, inters.get(k));
            }
            if (j == inters.size()) {
                loop = false;
            }
        }

        /* Return all intersections */
        return inters;
    }
}
