package com.mambre.IntersectingRectangles.managers;

import com.mambre.IntersectingRectangles.models.Intersection;
import com.mambre.IntersectingRectangles.enums.FileType;
import com.mambre.IntersectingRectangles.factories.ParserAbstractFactory;
import com.mambre.IntersectingRectangles.interfaces.IFile;
import com.mambre.IntersectingRectangles.interfaces.IParser;
import com.mambre.IntersectingRectangles.models.Rectangle;

import java.util.ArrayList;

public class Manager {
    public ArrayList<Rectangle> parse(String filename) {
        FileType type;
        if (filename.endsWith(".json")) {
            type = FileType.JSON;
        } else {
            type = FileType.XML; //for example
        }

        /* Get the corresponding parser */
        ParserAbstractFactory factory = ParserAbstractFactory.getFactory(type);
        IParser parser = factory.getParser();

        IFile file = parser.getFile(filename);

        /* Check the validity of the file */
        if (!file.validContent() || !file.validExtension()) {
            return null;
        }

        /* Get data from the file */
        Object content = file.readData();

        /* Return all rectangles from the file */
        return parser.getRectangles(content, "rects", 10);
    }

    public ArrayList<Intersection> getIntersections(ArrayList<Rectangle> rects) {
        return new ManageRectangle().search(rects);
    }
}
