package com.mambre.IntersectingRectangles.models;

import java.util.ArrayList;
import java.util.Collections;

public class Intersection extends Rectangle {

    /* Store ids of all rectangle that create this intersection */
    private ArrayList<Integer> ids = new ArrayList<>();

    /* An intersection is created when two rectangles collapse */
    public Intersection(Rectangle A, Rectangle B) {
        /* Get the biggest x on the left down point */
        Integer x = Math.max(A.getLeftDown().getX(), B.getLeftDown().getX());

        /* Get the biggest y on the left down point */
        Integer y = Math.max(A.getLeftDown().getY(), B.getLeftDown().getY());

        /* Get the smallest x on the right down point minus the new x found */
        Integer width = Math.min(A.getRightDown().getX(), B.getRightDown().getX()) - x;

        /* Get the smallest y on the left up point minus the new y found */
        Integer height = Math.min(A.getLeftUp().getY(), B.getLeftUp().getY()) - y;

        /* Set the new rectangle */
        setLeftDown(new Point(x, y));
        setLeftUp(new Point(x, y + height));
        setRightDown(new Point(x + width, y));

        /* Add ids of rectangles which intersect between them */
        if (!ids.contains(A.getId())) {
            ids.add(A.getId());
        }
        if (!ids.contains(B.getId())) {
            ids.add(B.getId());
        }

        /* Sort ids */
        Collections.sort(ids);
    }

    public Intersection(Intersection i, Rectangle r) {
        /* Get the biggest x on the left down point */
        Integer x = Math.max(i.getLeftDown().getX(), r.getLeftDown().getX());

        /* Get the biggest y on the left down point */
        Integer y = Math.max(i.getLeftDown().getY(), r.getLeftDown().getY());

        /* Get the smallest x on the right down point minus the new x found */
        Integer width = Math.min(i.getRightDown().getX(), r.getRightDown().getX()) - x;

        /* Get the smallest y on the left up point minus the new y found */
        Integer height = Math.min(i.getLeftUp().getY(), r.getLeftUp().getY()) - y;

        /* Set the new rectangle */
        setLeftDown(new Point(x, y));
        setLeftUp(new Point(x, y + height));
        setRightDown(new Point(x + width, y));

        /* Add ids of rectangles which intersect between them */
        for (int j = 0; j < i.getIds().size(); j++) {
            if (!ids.contains(i.getIds().get(j))) {
                ids.add(i.getIds().get(j));
            }
        }
        if (!ids.contains(r.getId())) {
            ids.add(r.getId());
        }

        /* Sort ids */
        Collections.sort(ids);
    }

    public void delete() {
        setId(getI() - 1);
        setI(getI() - 1);
    }

    public void delete(Integer id) {
        setId(getI() - 1);
        setI(getI() - 1);
        getIds().remove(id);
        setIds(getIds());
    }

    public boolean isEqualsTo(Intersection i) {
        return ((getLeftUp().getX().equals(i.getLeftUp().getX()) && getLeftUp().getY().equals(i.getLeftUp().getY())
                && getLeftDown().getX().equals(i.getLeftDown().getX()) && getLeftDown().getY().equals(i.getLeftDown().getY())
                && getRightDown().getX().equals(i.getRightDown().getX()) && getRightDown().getY().equals(i.getRightDown().getY()))
                && getIds().equals(i.getIds()));
    }

    @Override
    public String toString() {
        String idsRectangles = "";
        for (int i = 0; i < ids.size(); i++) {
            idsRectangles += ids.get(i);
            if (ids.size() - 1 != i)
                idsRectangles += (i + 1 != ids.size() - 1) ? ", " : " and ";
        }

        return ": Between rectangle " + idsRectangles
                + " at (" + getLeftDown().getX()
                + "," + getLeftDown().getY()
                + "), w=" + (getRightDown().getX() - getLeftDown().getX())
                + ", h=" + (getLeftUp().getY() - getLeftDown().getY());
    }

    /** Setters and getters **/

    private ArrayList<Integer> getIds() {
        return ids;
    }

    private void setIds(ArrayList<Integer> ids) {
        this.ids = ids;
    }
}
