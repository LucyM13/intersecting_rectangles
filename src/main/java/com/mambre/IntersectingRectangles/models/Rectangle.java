package com.mambre.IntersectingRectangles.models;

public class Rectangle {
    private static Integer i = 1;

    private Integer id;
    private Point leftUp;
    private Point leftDown;
    private Point rightDown;

    protected Rectangle() {
        this.id = i++;
    }

    public Rectangle(Integer x, Integer y, Integer width, Integer height) {
        this.id = i++;
        this.leftDown = new Point(x, y);
        this.rightDown = new Point(x + width, y);
        this.leftUp = new Point(x, y + height);
    }

    /** Know if RectA has a intersection with RectB **/

    public Boolean hasIntersectionWith(Rectangle r) {
        if (getLeftUp().getX() > r.getRightDown().getX() || r.getLeftUp().getX() > getRightDown().getX()) {
            return false;
        }
        return (!(getLeftUp().getY() < r.getRightDown().getY() || r.getLeftUp().getY() < getRightDown().getY()));
    }

    /** Know if RectA is equals to RectB **/

    public Boolean isEqualsTo(Rectangle r) {
        return (getLeftUp().getX().equals(r.getLeftUp().getX()) && getLeftUp().getY().equals(r.getLeftUp().getY())
                && getLeftDown().getX().equals(r.getLeftDown().getX()) && getLeftDown().getY().equals(r.getLeftDown().getY())
                && getRightDown().getX().equals(r.getRightDown().getX()) && getRightDown().getY().equals(r.getRightDown().getY()));
    }

    /** Get information from a rectangle **/

    @Override
    public String toString() {
        return ": Rectangle at ("
                + getLeftDown().getX()
                + "," + getLeftDown().getY()
                + "), w=" + (getRightDown().getX() - getLeftDown().getX())
                + ", h=" + (getLeftUp().getY() - getLeftDown().getY()) + ".";
    }

    protected static Integer getI() {
        return i;
    }

    protected static void setI(Integer i) {
        Rectangle.i = i;
    }

    /** Setters and getters **/

    public Integer getId() {
        return id;
    }

    protected void setId(Integer id) {
        this.id = id;
    }

    public Point getLeftUp() {
        return leftUp;
    }

    protected void setLeftUp(Point leftUp) {
        this.leftUp = leftUp;
    }

    public Point getLeftDown() {
        return leftDown;
    }

    protected void setLeftDown(Point leftDown) {
        this.leftDown = leftDown;
    }

    public Point getRightDown() {
        return rightDown;
    }

    protected void setRightDown(Point rightDown) {
        this.rightDown = rightDown;
    }

    public Integer getWidth() {
        return getRightDown().getX() - getLeftDown().getX();
    }

    public Integer getHeight() {
        return getLeftUp().getY() - getLeftDown().getY();
    }
}
